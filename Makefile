install:
	go get -u \
		google.golang.org/grpc \
		github.com/golang/protobuf/protoc-gen-go
		
generate:
	protoc -I proto/ --go_out=plugins=grpc:proto proto/msgs.proto 
	protoc --proto_path=proto --js_out=import_style=commonjs,binary:client/src/proto/ --grpc-web_out=import_style=commonjs,mode=grpcwebtext:client/src/proto/ proto/*.proto

