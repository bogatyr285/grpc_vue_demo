package main

import (
	"flag"
	"log"
	"os"
	"os/signal"

	ServerFactory "bitbucket.org/bogatyr285/grpc_vue_demo/server/api"
)

func main() {
	//add file name to log output
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	gRPCPort := flag.String("port", "9090", "Which port we'll listen to")
	gRPCAddr := flag.String("addr", "0.0.0.0", "Which address we'll listen to")
	flag.Parse()

	server, err := ServerFactory.NewServer(*gRPCAddr, *gRPCPort)
	if err != nil {
		log.Fatalf("Failed to inialize gRPC server: %v", err)
	}
	go func() {
		server.Start()
	}()

	//Wait Ctrl+C to exit
	interruptCh := make(chan os.Signal, 1)
	signal.Notify(interruptCh, os.Interrupt)

	//Block until a signal to received
	<-interruptCh
	server.Stop()

}
