package srv

import (
	"fmt"
	"log"
	"net"

	pb "bitbucket.org/bogatyr285/grpc_vue_demo/proto"
	"google.golang.org/grpc"
)

// Server - structure describes gRPC props
type GRPCServer struct {
	Addr       string //domain name or just IP addr
	Port       string
	PathToCert string
	Listener   net.Listener
	GrpcSrv    *grpc.Server
	Tewst      string
}

// Start - run the gRPC server
func (s *GRPCServer) Start() {
	//TLS disabled because of proxy usage
	// creds, err := credentials.NewServerTLSFromFile("cert/server.crt", "cert/server.key")
	// if err != nil {
	// 	log.Fatalf("Could not load TLS keys: %s", err)
	// }

	// opts := []grpc.ServerOption{grpc.Creds(creds)}

	s.GrpcSrv = grpc.NewServer( /* opts... */ )
	pb.RegisterMsgsServiceServer(s.GrpcSrv, s)
	log.Printf("gRPC server started on %v", s.Addr)
	log.Fatal(s.GrpcSrv.Serve(s.Listener))
}

//Stop - gracefully stop server & listeners
func (s *GRPCServer) Stop() {
	log.Println("Stopping the server...")
	s.GrpcSrv.Stop()
	log.Println("Closing the listener...")
	s.Listener.Close()
	log.Println("Everything gracefully stopped")
}

// NewServer - initialize a new gRPC server instance
func NewServer(addr, port string) (*GRPCServer, error) {
	var server GRPCServer

	server.Port = port
	server.Addr = fmt.Sprintf("%s:%s", addr, port)

	tcpLstnr, err := net.Listen("tcp", server.Addr)
	if err != nil {
		return &server, err
	}
	server.Listener = tcpLstnr

	return &server, nil
}
