package srv

import (
	"context"
	"fmt"
	"io"
	"log"
	"strconv"
	"time"

	pb "bitbucket.org/bogatyr285/grpc_vue_demo/proto"
)

func (s *GRPCServer) SendMsg(ctx context.Context, req *pb.MsgRequest) (*pb.MsgReply, error) {
	log.Printf("[SendMsg.Unary]Received: %+v", req)
	reply := fmt.Sprintf("Accepted:%s from %s", req.Body, req.From)
	return &pb.MsgReply{Message: reply}, nil
}

func (s *GRPCServer) SendStreamMsgs(req *pb.StreamMsgRequest, stream pb.MsgsService_SendStreamMsgsServer) error {
	log.Printf("[SendStreamingMsgs.SrvStream] Received: %+v", req)
	for i := 0; i < 3; i++ {
		result := fmt.Sprintf("For req: %s I'll sent %s", req.Body, strconv.Itoa(i))
		res := &pb.StreamMsgResponse{
			Message: result,
		}
		stream.Send(res)
		time.Sleep(1 * time.Second)
	}
	return nil
}

func (s *GRPCServer) SendLotsOfMsgs(stream pb.MsgsService_SendLotsOfMsgsServer) error {
	log.Println("[SendLotsOfMsgs.ClnStream]Received request")
	result := ""
	for {
		req, err := stream.Recv()
		if err == io.EOF { //end of client stream, send our answer
			return stream.SendAndClose(&pb.StreamMsgResponse{
				Message: result,
			})
		}
		if err != nil {
			return err
		}
		result += fmt.Sprintf("Server've got %+v\n", req.GetBody())
	}
}
