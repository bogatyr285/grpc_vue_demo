# gRPC server(golang) and client (Vue) development environment with hot-reloads
Ready to use web development environment with hot-reload/recompile of client & server side deployed via docker-compose. Includes gRPC server on Go with web client based on VueJS. Also Envoy is used as proxy for GRPC->HTTP.

> Original server [repo]( https://bitbucket.org/bogatyr285/grpc-demo)
### Intallation

Deploy development environment (run server, client and proxy)
```
docker-compose up --build
```

### Compiling the proto file
Following command will generate compiled proto files for server and client applications:
```
make generate
```