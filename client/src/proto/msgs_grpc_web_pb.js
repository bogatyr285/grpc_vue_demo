/**
 * @fileoverview gRPC-Web generated client stub for api
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.api = require('./msgs_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.api.MsgsServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.api.MsgsServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.MsgRequest,
 *   !proto.api.MsgReply>}
 */
const methodInfo_MsgsService_SendMsg = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.MsgReply,
  /** @param {!proto.api.MsgRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.MsgReply.deserializeBinary
);


/**
 * @param {!proto.api.MsgRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.MsgReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.MsgReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.MsgsServiceClient.prototype.sendMsg =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.MsgsService/SendMsg',
      request,
      metadata || {},
      methodInfo_MsgsService_SendMsg,
      callback);
};


/**
 * @param {!proto.api.MsgRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.MsgReply>}
 *     A native promise that resolves to the response
 */
proto.api.MsgsServicePromiseClient.prototype.sendMsg =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.MsgsService/SendMsg',
      request,
      metadata || {},
      methodInfo_MsgsService_SendMsg);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.StreamMsgRequest,
 *   !proto.api.StreamMsgResponse>}
 */
const methodInfo_MsgsService_SendStreamMsgs = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.StreamMsgResponse,
  /** @param {!proto.api.StreamMsgRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.StreamMsgResponse.deserializeBinary
);


/**
 * @param {!proto.api.StreamMsgRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.StreamMsgResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.MsgsServiceClient.prototype.sendStreamMsgs =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.MsgsService/SendStreamMsgs',
      request,
      metadata || {},
      methodInfo_MsgsService_SendStreamMsgs);
};


/**
 * @param {!proto.api.StreamMsgRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.StreamMsgResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.MsgsServicePromiseClient.prototype.sendStreamMsgs =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.MsgsService/SendStreamMsgs',
      request,
      metadata || {},
      methodInfo_MsgsService_SendStreamMsgs);
};


module.exports = proto.api;

