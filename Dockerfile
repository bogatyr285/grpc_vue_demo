FROM golang:1.12.7-alpine3.10

WORKDIR $GOPATH/src/bitbucket.org/bogatyr285/grpc_vue_demo/

COPY server .
COPY proto .

RUN apk update && apk upgrade && \
    apk add --no-cache bash git 

RUN go get github.com/githubnemo/CompileDaemon 
RUN go get -u \
		google.golang.org/grpc \
		github.com/golang/protobuf/protoc-gen-go

ENTRYPOINT CompileDaemon -log-prefix=false -build="go build -o grpcsrv $GOPATH/src/bitbucket.org/bogatyr285/grpc_vue_demo/server" -command="./grpcsrv"